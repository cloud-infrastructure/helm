# Deploying Tungsten Fabric with Helm

The following guide will walk you through all the necessary steps to deploy Tungsten Fabric with Helm charts. This guide assumes the existence of an independent OpenStack infrastructure.

Using the charts provided at the [contrail-helm-deployer](https://github.com/Juniper/contrail-helm-deployer) repository, TF control plane will be deployed on a Magnum-powered Kubernetes cluster. In other words, all TF components but the vRouter component will run on a Kubernetes cluster. The vRouter will be managed separately as we currently do not support Kubernetes on our OpenStack compute nodes.

## Create Kubernetes cluster using Magnum

Following the [Cloud Docs Containers Quickstart](https://clouddocs.web.cern.ch/clouddocs/containers/quickstart.html) it is possible to launch a new Kubernetes cluster in just a few easy commands using Magnum.

## Setup Helm for the new cluster

Once the new Kubernetes cluster is successfully created, and loaded its configuration (_very important as Helm will install Tiller in the cluster extracted from $HOME/.kube/config_), it's time to install and configure Helm for new the cluster.

### Download Helm

First of all, Helm has to be downloaded. There are many possible ways which can be found at [Helm's installation guide](https://docs.helm.sh/using_helm/#installing-helm). Good news is that Helm client can be installed anywhere, as long as the access to the new Kubernetes cluster is correctly configured from the machine.

### Initialize Helm client and Tiller server

Initialize the Helm client running:

```
helm init
```

In the process, Helm will install its server called _Tiller_ in a single-container pod that will run on the _kube-system_ namespace of Kubernetes. TIller will be the place where Helm charts are stored and available for the Kubernetes cluster.

### Setup Helm local repository

Although this is not mandatory for Helm deployments, in the case of TF this is needed due a dependency on the [helm-toolkit-contrail](https://gitlab.cern.ch/cloud-infrastructure/contrail-helm-deployer/tree/master/helm-toolkit-contrail) chart.

```
helm serve &
helm repo add local http://localhost:8879/charts
helm repo remove stable
```

## Load all the TF's charts to Helm's local repository

From the root folder of [contrail-helm-deployer](https://github.com/Juniper/contrail-helm-deployer) repository, run the following command:

```
make
```

Re-running this command will be needed after applying changes to any Helm chart.

## Configure RBAC for the new Kubernetes cluster

Create a Kubernetes ClusterRoleBinding by running:

```
kubectl replace -f ${CHD_PATH}/rbac/cluster-admin.yaml
```

Where _$CHD_PATH_ is the path where the _contrail-helm-deployer_ repository has been cloned.

## Label the Kubernetes nodes

Label the minion nodes in order to deploy RabbitMQ issuing the following command:

```
kubectl label node <minion_node> openstack-control-plane=enabled
```

Label the minion nodes in order to deploy the control plane components of TF:

```
kubectl label node <minion_node> opencontrail.org/controller=enabled
```

## Configure NTP in the Kubernetes nodes (TODO)

TF services requires all the nodes to be in NTP synchronized state. The nodemgr starts monitoring the ntp state few minutes after it is up and reports if the NTP is not synchronized. It determines this by using `ntpq -n -c pe | grep "^*"` output.


## Deploy RabbitMQ

TF requires RabbitMQ for internal communication among its control plane components. This can be easily done using RabbitMQ's Helm chart from the [openstack-helm](https://github.com/openstack/openstack-helm) project.

```
helm install --name=rabbitmq ${OSH_PATH}/rabbitmq --namespace=openstack --set volume.enabled=false
```

Where _$OSH_PATH_ is the path where the _openstack-helm_ repository has been cloned.

## Deploy Tungsten Fabric charts

### Deploy contrail-thirdparty applications

Run the following command in order to deploy thirdparty applications required by TF such as Cassandra, Redis, Kafka or Zookeeper.

```
helm install --name contrail-thirdparty ${CHD_PATH}/contrail-thirdparty --namespace=contrail -f values/thirdparty.yaml -f secrets/thirdparty.yaml
```

Where _$CHD_PATH_ is the path where the _contrail-helm-deployer_ repository has been cloned.

### Deploy contrail-controller applications

Run the following command in order to deploy the controller "bundle" of TF. This bundle is composed of the config, control, and webui pods.

```
helm install --name contrail-controller ${CHD_PATH}/contrail-controller --namespace=contrail -f values/controller.yaml -f secrets/controller.yaml
```

Where _$CHD_PATH_ is the path where the _contrail-helm-deployer_ repository has been cloned.

### Deploy contrail-analytics applications

Run the following command in order to deploy the analytics pod of TF.

```
helm install --name contrail-analytics ${CHD_PATH}/contrail-analytics --namespace=contrail -f values/analytics.yaml -f secrets/analytics.yaml
```

Where _$CHD_PATH_ is the path where the _contrail-helm-deployer_ repository has been cloned.
